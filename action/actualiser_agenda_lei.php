<?php

/**
 * Gestion de l'action actualiser_agenda_lei
 * 
 * @package SPIP\Lei\Actions
 */
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Actualise les données de l'agenda LEI
 *
 * @see agenda_lei_update()
 * @return void
**/
function action_actualiser_agenda_lei_dist() {
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	if (!autoriser('configurer')) {
		include_spip('inc/minipres');
		minipres();
		exit;
	}

	include_spip('inc/agenda_lei_update');
	agenda_lei_update();
}

?>
