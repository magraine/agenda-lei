<?php
/**
 * Gestion de l'installation / désinstallation
 * 
 * @package SPIP\Lei\Installation
 */

include_spip('inc/meta');
include_spip('base/create');


/**
 * Installation/maj des tables spip_lei_evenements
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
 */
function lei_upgrade($nom_meta_base_version, $version_cible){

	include_spip('inc/config');
	include_spip('base/create');

	$maj = array();
	$maj['create'] = array(
		array('maj_tables', array('spip_lei_evenements')),
	);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Désinstallation/suppression des tables spip_lei_evenements
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
 */
function lei_vider_tables($nom_meta_base_version) {
	sql_drop_table("spip_lei_evenements");

	effacer_meta('agenda_lei');
	effacer_meta($nom_meta_base_version);
}

