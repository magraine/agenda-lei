<?php

/**
 * Utilisation de pipelines
 *
 * @package SPIP\Lei\Pipelines
**/

if (!defined("_DUREE_CRON_AGENDA_LEI")) {
	/**
	 * Actualisation des données de l'agenda LEI
	 * @var int Durée entre chaque actualisation, en secondes */
	define("_DUREE_CRON_AGENDA_LEI", 3600 * 8);
}


/**
 * Ajouter la tache de récupérer les infos de l'agenda LEI
 * toutes les 8 heures... 
 *
 * @param array $taches
 *     Tableau Liste des taches / durée 
 * @return
 *     Tableau de la liste des taches complété.
**/
function lei_taches_generales_cron($taches){
	$taches['recuperer_agenda_lei_distant'] = _DUREE_CRON_AGENDA_LEI; // toutes les 8 heures
	return $taches;
}



