<?php

/**
 * Gestion du génie d'actualisation de l'agenda LEI
 * 
 * @package SPIP\Lei\Genie
 */
 
/**
 * Actualisation des tables de l'agenda lei SPIP 
 *
 * @param int $t
 *     Timestamp de la date de dernier appel de la tâche
 * @return int
 *     ?
**/
function genie_recuperer_agenda_lei_distant_dist($t){
	
	// nombre de jours de conservation
	spip_log("Récupération de l'agenda LEI distant...");

	include_spip('inc/agenda_lei_update');
	agenda_lei_update();

	include_spip('inc/invalideur');
	suivre_invalideur(1); // perimer le cache entier de SPIP
	
	return 1;
}





?>
