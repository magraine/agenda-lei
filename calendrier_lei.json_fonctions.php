<?php

/**
 * Fichier de fonction du json du calendrier LEI
 *
 * @package SPIP\Lei\Fonctions
**/
if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('calendrier_mini.json_fonctions');
