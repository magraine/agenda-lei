<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'actualiser_derniere_actualisation' => "Dernière actualisation",
	'actualiser_agenda'    => "Actualiser l'agenda",
	'aucune_manifestation' => "Aucune manifestation",
	'agenda_lei'           => "Agenda LEI",
	'agenda_lei_introduction' => "
		Cette page permet de lister des éléments de l'agenda LEI
		qui ont été récupérés et enregistrés sur le site. Elle permet aussi
		pour les administrateurs de forcer une mise à jour des données. 
	",

	// I
	'info_reussite'      => "Opération réussie?",
	'info_temps_total'   => "Temps total&nbsp;:",
	'info_temps_xml'     => "Temps récup XML&nbsp;:",
	'info_temps_parsage' => "Temps parsage XML&nbsp;:",
	'info_temps_insertions' => "Temps insertions&nbsp;:",
	'info_nb_item'       => "Nb. item&nbsp;:",
	'info_nb_insertions'       => "Nb. insertion&nbsp;:",
	'info_date'          => "Date&nbsp;:",
	
	// L
	'label_nom'      => "Nom",
	'label_date'     => "Date",
	'label_date_debut'   => "Début",
	'label_date_fin'     => "Fin",
	
	// M
	'manifestations' => "Manifestations",

	// P
	'periode' => "Période",

	// S
	'selection_du_calendrier' => "Sélection du calendrier",

);

?>
