<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-lei
// Langue: fr
// Date: 13-09-2012 11:53:47
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// L
	'lei_description' => 'Fournit des fonctionnalités de visualisation
	d\'un agenda issu de LEI.',
	'lei_slogan' => 'Fournit des fonctionnalités de visualisation',
);
?>