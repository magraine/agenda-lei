<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// I
	'info_nb_lei_evenements' => '@nb@ événements LEI',
	'info_1_lei_evenement' => 'Un événement LEI',
	'info_aucun_lei_evenement' => 'Aucun événement LEI',


	// L
	'label_adresse' => 'Adresse',
	'label_agrements' => 'Agréments',
	'label_animations' => 'Animations',
	'label_animaux' => 'Animaux',
	'label_autocar' => 'Autocar',
	'label_capacite' => 'Capacité',
	'label_cc' => 'Cc',
	'label_civilite' => 'Civilité',
	'label_codetype' => 'Code type',
	'label_com1' => 'Commentaire 1',
	'label_com2' => 'Commentaire 2',
	'label_date_debut' => 'Date debut',
	'label_date_fin' => 'Date fin',
	'label_distances' => 'Distances',
	'label_duree' => 'Durée',
	'label_encadrement' => 'Encadrement',
	'label_faxcontact' => 'Fax du contact',
	'label_gratuit' => 'Gratuit',
	'label_groupe' => 'Groupe',
	'label_handicap' => 'Handicap',
	'label_horaires' => 'Horaires',
	'label_image' => 'Image',
	'label_jour' => 'Jour',
	'label_langues' => 'Langues',
	'label_melcontact' => 'Courriel du contact',
	'label_nom' => 'Nom',
	'label_nomcontact' => 'Nom du contact',
	'label_nomtype' => 'Type de nom',
	'label_paiement' => 'Paiement',
	'label_parking' => 'Parking',
	'label_prenomcontact' => 'Prénom du contact',
	'label_produit' => 'Produit',
	'label_rando' => 'Randonnée',
	'label_rdv' => 'Rendez-vous',
	'label_resto1' => 'Resto 1',
	'label_resto2' => 'Resto 2',
	'label_sectgeo' => 'Secteur géographique',
	'label_sncf' => 'SNCF',
	'label_specif' => 'Spécificités',
	'label_tarifs' => 'Tarifs',
	'label_telcontact' => 'Téléphone du contact',
	'label_urlcontact' => 'Url du contact',
	'label_ville' => 'Ville',
	'label_visiteguide' => 'Visite guidée',
	'label_visitepayante' => 'Visite payante',

	// T
	'titre_lei_evenements_rubrique' => 'Les Événements LEI de la rubrique',
	'titre_lei_evenements' => 'Événements LEI',
	'titre_lei_evenement' => 'Événement LEI',
	'titre_logo_lei_evenement' => 'Logo de cet événement',

);

