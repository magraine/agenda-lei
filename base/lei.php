<?php

/**
 * Déclarations relatives à la base de données
 * 
 * @package SPIP\Lei\Pipelines
**/

/**
 * Déclarations des interfaces pour le compilateur
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interface
 *     Description des interfaces
 * @return array
 *     Description complétée des interfaces
 */
function lei_declarer_tables_interfaces($interface){
	// Alias de table pour les boucles (LEI_EVENEMENTS)
	$interface['table_des_tables']['lei_evenements'] = 'lei_evenements';
	return $interface;
}

/**
 * Déclarer les objets éditoriaux des événements de lei
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function lei_declarer_tables_objets_sql($tables) {

	//-- Table lettres
	$tables['spip_lei_evenements'] = array(
		'type' => 'lei_evenement',

		'titre' => "nom AS titre, '' AS lang",
		'date' => 'date_debut',
		'principale' => 'oui',
		'editable' => false,

		'field' => array(
			"id_lei_evenement" => "int(11) NOT NULL",
			"id_rubrique"  => "bigint(21) NOT NULL default '0'",
			"produit"      => "int(11) NOT NULL default '0'",
			"nom"          => "varchar(255) NOT NULL default ''",
			"nomtype"      => "varchar(200) NOT NULL default ''",
			"codetype"     => "varchar(255) NOT NULL default ''",
			
			"adresse"  => "varchar(255) NOT NULL default ''",
			"ville"    => "varchar(150) NOT NULL default ''",
			"com1"     => "longtext NOT NULL default ''",
			"com2"     => "longtext NOT NULL default ''",
			"civilite" => "varchar(20) NOT NULL default ''",
			
			"nomcontact"    => "varchar(200) NOT NULL default ''",
			"prenomcontact" => "varchar(200) NOT NULL default ''",
			"telcontact"    => "varchar(15) NOT NULL default ''",
			"faxcontact"    => "varchar(15) NOT NULL default ''",
			"melcontact"    => "varchar(200) NOT NULL default ''",
			"urlcontact"    => "varchar(200) NOT NULL default ''",
			
			"rdv"         => "varchar(50) NOT NULL default ''",
			"jour"        => "varchar(255) NOT NULL default ''",
			"horaires"    => "varchar(255) NOT NULL default ''",
			"specif"      => "varchar(255) NOT NULL default ''",
			"groupe"      => "varchar(255) NOT NULL default ''",
			"handicap"    => "varchar(50) NOT NULL default ''",
			"autocar"     => "varchar(255) NOT NULL default ''",
			"parking"     => "varchar(255) NOT NULL default ''",
			"resto1"      => "varchar(255) NOT NULL default ''",
			"resto2"      => "varchar(255) NOT NULL default ''",
			"capacite"    => "varchar(255) NOT NULL default ''",
			"animaux"     => "varchar(50) NOT NULL default ''",
			"langues"     => "varchar(255) NOT NULL default ''",
			"gratuit"     => "varchar(255) NOT NULL default ''",
			"tarifs"      => "varchar(255) NOT NULL default ''",
			"paiement"    => "varchar(255) NOT NULL default ''",
			"cc"          => "varchar(255) NOT NULL default ''",
			"sectgeo"     => "varchar(255) NOT NULL default ''",
			"distances"   => "varchar(255) NOT NULL default ''",
			"visiteguide" => "varchar(255) NOT NULL default ''",
			"duree"       => "varchar(255) NOT NULL default ''",
			"sncf"        => "varchar(255) NOT NULL default ''",
			"animations"  => "longtext NOT NULL default ''",
			"agrements"   => "varchar(255) NOT NULL default ''",
			"visitepayante" => "varchar(255) NOT NULL default ''",
			"encadrement"   => "varchar(255) NOT NULL default ''",
			"rando"         => "varchar(255) NOT NULL default ''",
			
			"date_debut" => "datetime NOT NULL default '0000-00-00 00:00:00'",
			"date_fin"   => "datetime NOT NULL default '0000-00-00 00:00:00'",
			"date"       => "datetime NOT NULL default '0000-00-00 00:00:00'",

			"image"  => "varchar(255) NOT NULL default ''",
		),
		'key' => array(
			"PRIMARY KEY"	=> "id_lei_evenement",
		),
		'rechercher_champs' => array(
			'nom' => 8,
			'ville' => 5,
			'nomcontact' => 1,
			'animations' => 3,
			'nomtype' => 3,
		)
	);

	return $tables;
}

