<?php

/**
 * Filtres et fonctions pour les squelettes
 * 
 * @package SPIP\Lei\Fonctions
 */

/**
 * Traduit une date au format jj/mm/yyyy en un format SQL
 *
 * @param string $datePicker Date au format jj/mm/yyyy
 * @param string $heurePicker Heure au format hh:mm
 * 
 * @return string Date formatée yyyy-mm-dd hh:mm:ss
**/
function filtre_lei_date_picker_to_date_dist($datePicker, $heurePicker = '00:00'){

	if (!$datePicker) {
		return false;
	}
	// $datePicker : jj/mm/yyyy
	if (!$date = recup_date($datePicker . ' ' . $heurePicker . ':00')) {
		return false;
	}

	if (!($date = mktime($date[3], $date[4], 0, (int)$date[1], (int)$date[2], (int)$date[0]))) {
	  // mauvais format de date
	  return false;
	}

	return date("Y-m-d H:i:s", $date);
}

