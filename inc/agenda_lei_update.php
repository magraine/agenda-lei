<?php

/**
 * Plugin LEI pour Spip 3.0
 * Licence GPL (c) 2010 - Matthieu Marcillaud / Pascal Mechin
 * ( Sources d'origine Michel-ADRT23. Presque tout réécrit )
 *
 * @package SPIP\Lei\Actualisation
 */
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Prendre le XML distant (ou un local mis en cache (le dernier reçu)
 * (pour tests locaux)
**/
define("_XML_AGENDA_DISTANT", true);

/**
 * Cache du fichier XML envoyé
 * (pour tests locaux)
**/
define("_CACHE_XML_AGENDA_LEI", _DIR_TMP . 'sources_xml_lei.xml');


/**
 * URL du XML (source des données distantes de l'agenda LEI)
 * [TODO] URL à passer en config ?
**/
define("_URL_XML_AGENDA_LEI", 
	  "http://www.tourisme-limousin.net/xml/exploitation/listeproduits.asp?"
	. "rfrom=1&"
	. "rto=1000&"
	. "user=2000006&"
	. "pwkey=5bf414a7166ba982769aac526c75ce7e&"
	. "urlnames=tous&"
	. "PVALUES=30000006%2CPOC%2C2000006%2C04%2F03%2F2010+00%3A00%3A00%2C01%2F01%2F2100+23%3A59%3A59&"
	. "PNAMES=elgendro%2Celsector%2Cutilisador%2Chorariodu%2Chorarioau&"
	. "clause=2000006000141"
);


/**
 * Item des champs a récupérer
 * du fichier XML de l'agenda LEI
 * 
**/
define("_ITEM_XML_AGENDA_LEI", "sit_liste");


/**
 * Liste des champs a récupérer
 * du fichier XML de l'agenda LEI
 *
 * Est serialisé car une constante n'aime pas des array()...
**/
define("_CHAMPS_XML_AGENDA_LEI", serialize(array(
	"NOM",
	"PRODUIT",
	"HORAIRES",
	"ADRPROD_LIBELLE_COMMUNE",
	"TYPE_NOM",
	"ADRPROD_CP",
	"CIVILITE_PERSONNE_EN_CHARGE",
	"PRENOM_PERSONNE_EN_CHARGE",
	"NOM_PERSONNE_EN_CHARGE",
	"ADRPROD_TEL",
	"ADRPROD_FAX",
	"ADRPROD_EMAIL",
	"ADRPROD_URL",
	"COMMENTAIRE",
	"COMMENTAIREL1",
	"COMMENTAIREL2",
	"TYPE_DE_PRODUIT",
	"REFERENCE","CRITERES",
	"ADRPROD_COMPL_ADRESSE"
)));



/**
 * Fonction principale de mise à jour de l'agenda :
 * Récupère les infos XML distantes
 * et met à jour les informations dans la base de donnée locale. 
 *
 * @return bool Opération réussie ?
**/
function agenda_lei_update() {

	// chronomètre...
	$start = time();
	
	// réussite ?
	$ok = true;
	
	// recuperation du XML
	if (_XML_AGENDA_DISTANT) {
		$xml = file_get_contents(_URL_XML_AGENDA_LEI);
		ecrire_fichier(_CACHE_XML_AGENDA_LEI, $xml);
	} else {
		$xml = file_get_contents(_CACHE_XML_AGENDA_LEI);
	}
	
	$time_xml = time() - $start;
	
	if (!$xml) {
		$ok = false;
		spip_log("AGENDA LEI: Impossible d'accéder au fichier XML source.");
		
	} else {
		
		// parsage du XML
		$time_tmp = time();
		$xml = lei_lire_xml($xml, _ITEM_XML_AGENDA_LEI, unserialize(_CHAMPS_XML_AGENDA_LEI));
		$time_parsage = time() - $time_tmp;
		
		if (false === $xml) {
			$ok = false;
			spip_log("AGENDA LEI: Impossible de comprendre le fichier XML source.");
			
		} else {
			
			// analyse des reponses...
			$time_tmp = time();
			$nb_insertions = 0;
			if ($nb_items = count($xml)) {
				
				// vider les tables...
				lei_vider_tables();
				
				// inserer les nouvelles donnees.
				foreach ($xml as $item) {
					$nb_insertions += lei_inserer_item($item);
				}
			}
			$time_insertions = time() - $time_tmp;
		}
	}
	
	$time_total = time() - $start;
	
	ecrire_meta('agenda_lei', serialize(array('times' => array(
		'date' => date('Y-m-d H:i:s'),
		'reussite' => $ok,
		'total' => $time_total,
		'xml' => $time_xml,
		'parsage' => isset($time_parsage) ? $time_parsage : 0,
		'insertion' => isset($time_insertions) ? $time_insertions : 0,
		'nb_items' => isset($nb_items) ? $nb_items : 0,
		'nb_insertions' => isset($nb_insertions) ? $nb_insertions : 0,
	))));

	include_spip('inc/invalideur');
	suivre_invalideur(1);
}


/**
 * Vide les tables SQL locales de l'agenda LEI 
 *
 * @return true
**/
function lei_vider_tables() {
	sql_query("TRUNCATE TABLE spip_lei_evenements");
	return true;
}


/**
 * Parse un XML de type agenda LEI
 *
 * (code en partie repris des fichiers fournis)
 * 
 * @param string $xml   Source XML (texte)
 * @param string $item  Délimiteur des informations (<item> ... </item>)
 * @param array $champs Champs a recuperer dans chaque item
 * @return Tableau des résultats trouvés. (false si erreur).
**/
function lei_lire_xml($xml, $item, $champs) {
	if ($xml and $chaine = preg_replace("/\r?\n/", '', $xml)) {
		$resultats = array();
		$chaine = str_replace(
			array("<![CDATA[", "]]>"),
			array("","") , $chaine);
		$items = preg_split("/<\/?".$item.">/", $chaine);
		for ( $i=1; $i < sizeof($items)-1; $i+=2) {
			$resultats[$i-1] = array();
			foreach ($champs as $champ) {
				$infos_champ = preg_split("/<\/?".$champ.">/", $items[$i]);
				$resultats[$i-1][] = @$infos_champ[1];
			}
		}
		return $resultats;
	}
   return false;
}




/**
 * Insère les informations d'un élément de l'agenda XML
 * dans la base de donnée locale.
 *
 * @note
 *     Code en partie repris des fichiers fournis
 *     mais tout de même modifié... parce que... fiou...
 *     on a fait beaucoup beaucoup plus lisible !
 *
 * @param string $item
 *     Item XML de l'élément
 * @return bool
 *     Opération réussie ?
 */
function lei_inserer_item($item) {
	$row = $item;
	$ladate = date("Y-m-d H:i:s");
	$nombre = 0;
	
	$horaire = $row[2];
	$horaire = str_replace('<Horaire>','',$horaire);
	$hh = explode('</Horaire>',$horaire);
	$horaire = "";
	for($i=0;$i<(sizeof($hh)-1);$i++) {
		$h1 = explode('</DATE_DEBUT>',$hh[$i]);
		$h2 = explode('<DATE_FIN fmt="DD/MM/YYYY">',$hh[$i]);
		$h1a = $h1[0];
		$h1a = str_replace('<DATE_DEBUT fmt="DD/MM/YYYY">','',$h1a);
		$h2a = $h2[1];
		$h2a = str_replace('</DATE_FIN>','',$h2a);
		$horaire .= $h1a."*".$h2a."#";
	}
	$data1 = explode("#",$horaire);
	$communeencours = lei_encode($row[3]);
	$monproduit = $row[1];
	$montype = lei_encode($row[4]);
	$montype = str_replace("&","et",$montype);
	$monnom = ucfirst(lei_encode($row[0]));
	$prenom = ucfirst(lei_encode($row[7]));
	$nom = strtoupper(lei_encode($row[8]));
	$adresse = lei_encode($row[19]." - ".$row[5]." - ".$row[3]);

	$comment1 = lei_encode($row[13]);
	$comment2 = lei_encode($row[14]);
	$comment3 = lei_encode($row[15]);

	$criteres = $row[18];
	$nomcontact = lei_encode($row[7]);
	$prenomcontact = lei_encode($row[8]);

	// Image
	$image = "";
	if (false !== $t = lei_get_xml_critere($criteres, "30000279", "30001667")) {
		$image = $t;
	}

	// RDV
	$rdv = "";
	if (false !== $t = lei_test_xml_criteres($criteres, array("30000359"=>array(
		"30002397" => "<b>Ev&egrave;nement hebdomadaire :</b> oui",
		"30002398" => "<b>Ev&egrave;nement hebdomadaire :</b> non",
	)))) {
		$rdv = $t;
	}
	
	// Jour de la semaine
	$jour = "";
	if (false !== $t = lei_test_xml_criteres($criteres, array("30000376"=>array(
		"30002588" => " Lundi,",
		"30002589" => " Mardi,",
		"30002590" => " Mercredi,",
		"30002591" => " Jeudi,",
		"30002592" => " Vendredi,",
		"30002593" => " Samedi,",
		"30002594" => " Dimanche,",
	)))) {
		$jour = "<b>Jour de la semaine :</b> " . substr($t, 0, -1); 
	}
	
	// Horaires d'ouverture
	$horaires = "";
	if (false !== $t = lei_get_xml_criteres($criteres, array("30000282"=>array(
		"30001687" => "@t@ ",
		"30001688" => "&agrave; @t@,",
		"30001689" => "@t@ ",
		"30001690" => "&agrave; @t@,",
		"30002766" => "@t@ ",
		"30002767" => "&agrave; @t@,",
	)))) {
		$horaires = "<b>Horaires d'ouverture :</b> " . substr($t, 0, -1); 
	}
	
	// Groupes
	$groupes = "";
	if (false !== $t = lei_get_xml_criteres($criteres, array("30000275"=>array(
		"30001643" => "Mini : @t@,",
		"30001644" => "Maxi : @t@,",

	)))) {
		$groupes = "<b>Accueil de groupes :</b> " . substr($t, 0, -1); 
	}

	// Handicapés
	$hand = "non";
	if (false !== lei_test_xml_critere($criteres, "30000077", "30001180")) {
		$hand = "oui";
	}


	// Autocars
	$autocar = "";
	if (false !== $t = lei_test_xml_criteres($criteres, array("30000129"=>array(
		"30000305" => " &agrave; proximit&eacute;,",
		"30001507" => " &agrave; proximit&eacute;,",
		"30000304" => " sur place,"
	)))) {
		$autocar = "<b>Acc&egrave;s autocar :</b> " . substr($t, 0, -1); 
	}


	// Parking
	$parking = "";
	if (false !== $t = lei_test_xml_criteres($criteres, array("30000084"=>array(
		"30001437" => " oui,",
		"30001438" => " non,",
		"30001569" => " &agrave; proximit&eacute;,",
		"30001566" => " autocars,",
		"30001448" => " garage priv&eacute; gratuit,",
		"30001448" => " garage priv&eacute; gratuit,",
		"30000348" => " garage priv&eacute; payant,",
		"30000004" => " parking payant,",
		"30000003" => " parking gratuit,",
		"30000002" => " parking plein ciel,",
		"30000005" => " local &agrave; deux roues,",
	)))) {
		$parking = "<b>Stationnement et parking :</b> " . substr($t, 0, -1); 
	}


	// Restaurant
	$resto = "";
	if (false !== lei_test_xml_critere($criteres, "30000051", "30000063")) {
		$resto = "<b>Restaurant :</b> oui";
	}


	// Restauration
	$restauration = "";
	if (false !== $t = lei_test_xml_criteres($criteres, array("30000099"=>array(
		"30000163" => " gastronomique,",
		"30001401" => " r&eacute;gionale,",
		"30001417" => " plats cuisin&eacute;s,",
		"30000524" => " plats emport&eacute;s,",
		"30002726" => " sur place,",
	)))) {
		$restauration = "<b>Restauration :</b> " . substr($t, 0, -1); 
	}
		

	// Capacité de personnes
	$capacitepers = "";
	if (false !== $t = lei_get_xml_critere($criteres, "30000172", "0")) {
		$capacitepers = $t;
	}
	
	// Animaux autorisés ?
	$animaux = "non";
	if (false !== $t = lei_test_xml_critere($criteres, "30000014", "30000013")) {
		$animaux = "oui";
	}
	

	// Langues parlées
	$langues = "";
	if (false !== $t = lei_test_xml_criteres($criteres, array("30000010"=>array(
		"30000001" => " anglais,",
		"30000002" => " espagnol,",
		"30000003" => " italien,",
		"30000004" => " allemand,",
		"30000005" => " n&eacute;erlandais,",
	)))) {
		$langues = "<b>Langues parl&eacute;es :</b> " . substr($t, 0, -1); 
	}
	

	// Gratuités
	$gratuit = "";
	if (false !== $t = lei_test_xml_criteres($criteres, array("30000056"=>array(
		"30000075" => " Accompagnateurs,",
		"30000078" => " Enfants - de 12 ans,",
		"30000077" => " Pour le chauffeur,",
		"30000076" => " Pour tous,",
		"30000072" => " Non,",
	)))) {
		$gratuit = "<b>Gratuit&eacute;s :</b> " . substr($t, 0, -1); 
	}


	// Tarifs des prestations
	$tarifs = "";
	if (false !== $t = lei_get_xml_criteres($criteres, array("30000303"=>array(
		"30001880" => "Individuel enfant @t@,",
		"30001881" => "Individuel adulte @t@,",
		"30001885" => "Individuel &eacute;tudiant @t@,",
		"30001882" => "Groupe enfants (primaire) @t@,",
		"30001882" => "Famille @t@,",
		"30002533" => "Ch&ocirc;meurs @t@,",
		"30002533" => "Ch&ocirc;meurs @t@,",
	)))) {
		$tarifs = "<b>Tarifs prestations :</b> " . substr($t, 0, -1); 
	}


	// Paiements
	$paiements = "";
	if (false !== $t = lei_test_xml_criteres($criteres, array("30000100"=>array(
		"30000145" => " American express,",
		"30000144" => " Carte de cr&eacute;dit,",
		"30001841" => " Ch&egrave;que bancaire,",
		"30000146" => " Ch&egrave;que vacances,",
		"30000152" => " Diners,",
		"30001843" => " Esp&egrave;ces,",
		"30000151" => " Eurocard,",
		"30000150" => " Euroch&egrave;ques,",
		"30000149" => " Master-card,",
		"30001615" => " Tickets restaurant,",
		"30000148" => " Travellers,",
		"30001842" => " Virement,",
		"30000147" => " Visa,",

	)))) {
		$paiements = "<b>Paiements :</b> " . substr($t, 0, -1); 
	}


	// Distances
	$distances = "";
	if (false !== $t = lei_get_xml_criteres($criteres, array("30000369"=>array(
		"30002471" => "Paris : @t@km,",
		"30002472" => "Bordeaux : @t@km,",
		"30002473" => "Clermont-Ferrand : @t@km,",
		"30002474" => "Lille : @t@km,",
		"30002475" => "Lyon : @t@km,",
		"30002476" => "Marseille : @t@km,",
		"30002477" => "Nantes : @t@km,",
		"30002478" => "Toulouse : @t@km,",
	)))) {
		$distances = "<b>Distances villes de France :</b> " . substr($t, 0, -1); 
	}



	// Visite Guidée
	$visiteguide = "";
	if (false !== $t = lei_test_xml_criteres($criteres, array("30000048"=>array(
		"30001597" => " Oui sur RDV,",
		"30001571" => " Individuels,",
		"30001572" => " Groupes,",
		"30001574" => " Adultes,",
		"30001575" => " Enfants,",
		"30001570" => " Possible sur demande,",
		"30000055" => " Non,",

	)))) {
		$visiteguide = "<b>Visite guid&eacute;e :</b> " . substr($t, 0, -1); 
	}

	// Durée de la visite
	$duree = "";
	if (false !== $t = lei_get_xml_critere($criteres, "30000057", "0")) {
		$duree = "<b>Dur&eacute;e de la visite :</b> ";
		if ($t > 5 )
			$duree .= "$t minutes";
		else
			$duree .= "$t heures";	
	}	


	// Animations culturelles
	$animations = "";
	if (false !== $t = lei_test_xml_criteres($criteres, array("30000081"=>array(
		"30000007" => " Activit&eacute;s artistiques,",
		"30000534" => " Ateliers caligraphie,",
		"30001188" => " Exposition temporaire,",
		"30001189" => " Soir&eacute;es &agrave; th&egrave;me,",
		"30000006" => " Spectacles,",

	)))) {
		$animations = "<b>Animations culturelles :</b> " . substr($t, 0, -1); 
	}

	
	// Agréments
	$agrement = "";
	if (false !== $t = lei_test_xml_criteres($criteres, array("30000208"=>array(
		"30001478" => " Education nationale,",
		"30000158" => " Jeunesse & sports,",
		"30001963" => " DDASS,",
		"30002409" => " &eacute;tablissement recevant du Public,",
	)))) {
		$agrement = "<b>Agr&eacute;ments :</b> " . substr($t, 0, -1); 
	}


	// Visite payante...
	$visitepayante = "";
	if (false !== $t = lei_get_xml_criteres($criteres, array('30000269' => array(
		'30001651' => " Oui,",
		'30001893' => " Non,",
		'30001648' => " Tarif adulte : @t@&euro;,",
		'30001649' => " Tarif enfant : @t@&euro;,",
		'30001650' => " Tarif groupe adulte : @t@&euro;,",
	)))) {
		$visitepayante = "<b>Visite payante :</b> " . substr($t, 0, -1);
	}

	// Encadrement
	$encadrement = "";
	if (false !== $t = lei_test_xml_criteres($criteres, array('30000270' => array(
		'30001558' => " Accompagnateur,",
		'30001550' => " Accompagnateur de tourisme &eacute;questre,",
		'30002778' => " Brevet d'Animation Poney,",
		'30002246' => " Brevet d'apprentissage Poney,",
		'30001551' => " Brevet d'&eacute;tat,",
		'30001552' => " Brevet d'&eacute;tat et accompagnateur de tourisme &eacute;questre,",
		'30001553' => " Brevet d'&eacute;tat et guide de tourisme &eacute;questre,",
		'30002777' => " Brevet Fédéral d'Equitation Ethologique,",
		'30001555' => " Guide accompagnateur,",
		'30001559' => " Guide de tourisme &eacute;questre,",
		'30002248' => " Juge agr&eacute;&eacute;,",
		'30002177' => " Ma&icirc;tre nageur sauveteur,",
		'30002247' => " Ma&icirc;tre randonneur,",
		'30002241' => " Meneur de tourisme &eacute;questre,",
		'30002732' => " BPJEPS,",
		'30002733' => " BAPAAT,",
	)))) {
		$encadrement = "<b>Encadrement :</b> " . substr($t, 0, -1);
	}


	// Randonnée
	$rando = "";
	if (false !== $t = lei_test_xml_criteres($criteres, array('30000333' => array(
		'30002178' => " Balade (inf&eacute;rieur &Agrave; 8 km),",
		'30002179' => " Petite randonn&eacute;e (entre 8 et 30 km),",
		'30002180' => " Grande randonn&eacute;e,",
		'30002181' => " G.R.P.,",
		'30002860' => " Cyclo,",
		'30002857' => " Equestre,",
		'30002858' => " P&eacute;destre,",
		'30002859' => " VTT,",
		'30002861' => " Toutes,",
	)))) {
		$rando = "<b>Type de randonn&eacute;e :</b> " . substr($t, 0, -1);
	}


	// Gare de
	$sncf = "";
	if (false !== $t = lei_test_xml_criteres($criteres, array('30000066' => array(
		'30000406' => " Aubusson,",
		'30002070' => " Felletin,",
		'30000038' => " Gu&eacute;ret,",
		'30000404' => " La Souterraine,",
	)))) {
		$sncf = "<b>Gare de :</b> " . substr($t, 0, -1);
	}


	// Image
	$image = lei_get_xml_critere($criteres, "30000279", "30001667");
	if (!$image) { $image = lei_get_xml_critere($criteres, "30000279", "30001668"); }
	if (!$image) { $image = lei_get_xml_critere($criteres, "30000279", "30001669"); }
	if (!$image) { $image = lei_get_xml_critere($criteres, "30000279", "30001664"); }
	if (!$image) { $image = lei_get_xml_critere($criteres, "30000279", "30001665"); }
	if (!$image) { $image = lei_get_xml_critere($criteres, "30000279", "30001666"); }
	if (!$image) { $image = lei_get_xml_critere($criteres, "30000279", "30002828"); }
	if (!$image) { $image = lei_get_xml_critere($criteres, "30000279", "30002829"); }
	if (!$image) { $image = lei_get_xml_critere($criteres, "30000279", "30002830"); }
	if (!$image) { $image = ""; }

	
   for($i=0;$i<(sizeof($data1)-1);$i++) {
	   $toto = $data1[$i]."";
	   $data2 = explode("*",$toto);
	   $nombre++;
	   list($j,$m,$a) = explode("/",$data2[0]);
	   $du = $a."-".$m."-".$j;
	   list($j1,$m1,$a1) = explode("/",$data2[1]);
	   $au = $a1."-".$m1."-".$j1;

	   
	   sql_insertq('spip_lei_evenements', array(
			"id_rubrique" => 1,
			"produit"    => $monproduit,
			"codetype"   => $row[16],
			
			"nom"        => $monnom,
			"adresse"    => $adresse,
			"ville"      => $communeencours,
			
			"com1"       => $comment1,
			"com2"       => $comment2,
			
			"civilite"      => $row[6],
			"nomcontact"    => $prenomcontact,
			"prenomcontact" => $nomcontact,
			"telcontact"    => $row[9],
			"faxcontact"    => $row[10],
			"melcontact"    => $row[11],
			"urlcontact"    => $row[12],

			"rdv"        => $rdv,
			"jour"       => $jour,
			"horaires"   => $horaires,
			"date_debut" => $du,
			"date_fin"   => $au,
			
			"nomtype"    => $montype,
			
			"specif"     => $row[15],
			"groupe"     => $groupes,
			"handicap"   => $hand,
			"autocar"    => $autocar,
			"parking"    => $parking,
			"resto1"     => $resto,
			"resto2"     => $restauration,
			"capacite"   => $capacitepers,
			"animaux"    => $animaux,
			"langues"    => $langues,
			"gratuit"    => $gratuit,
			"tarifs"     => $tarifs,
			"paiement"   => $paiements,
			"cc"         => $row[28],
			"sectgeo"    => $row[29],
			"distances"  => $distances,
			
			"visiteguide"   => $visiteguide,
			"duree"         => $duree,
			"sncf"          => $sncf,
			"animations"    => $animations,
			"agrements"     => $agrement,
			"visitepayante" => $visitepayante,
			"encadrement"   => $encadrement,
			"rando"         => $rando,
			
			"date"          => $ladate,
			"image"         => $image
	   ));
	}
	return $nombre;
}



/**
 * Encode le xml pour la bdd 
 *
 * @param string $texte
 *     Texte à transformer dans le bon charset
 * @param bool $virer_guillemets
 *     Enlève les " si vrai.
 * @return string
 *     Texte transformé
**/
function lei_encode($texte, $virer_guillemets=false) {
	$texte = htmlspecialchars($texte, ENT_QUOTES);
/*  if ($virer_guillemets) {
		$texte = str_replace('"', "", $texte);
	} */
	return $texte;
}


/**
 * Fonction pour tester une info de critère 
 *
 * @param string $item
 *     Arbre xml de l'élément...
 * @param string $critere
 *     Identifiant du critère
 * @param string $moda
 *     Clé moda
 * @return bool
 *     true si présent, sinon false.
**/
function lei_test_xml_critere($item, $critere, $moda) {
	return preg_match('/Crit CLEF_CRITERE="'.$critere.'" CLEF_MODA="'.$moda.'">/i', $item);
}


/**
 * Fonction pour récupérer une info de critère 
 *
 * @param string $item
 *     Arbre xml de l'élément...
 * @param string $critere
 *     Identifiant du critère
 * @param string $moda
 *     Clé moda
 * @return bool
 *     Texte trouvé, sinon false.
**/
function lei_get_xml_critere($item, $critere, $moda) {
	if (lei_test_xml_critere($item, $critere, $moda)) {
		$c = explode('<Crit CLEF_CRITERE="'.$critere.'" CLEF_MODA="'.$moda.'">',$item);
		$c = explode('</Crit>',$c[1]);
		return $c[0];  
	}
	return false;
}

/**
 * Cherche parmis plusieurs criteres de sélection
 *
 * @param string $item
 *     Arbre xml de l'élément...
 * @param array $search
 *     Critères que l'on souhaite sélectionner
 *     $search = array(
 *         $critere => array(
 *             $moda => "Texte avec @t@ remplacé par ce qui a été trouvé"))
 * 
 * @return string|bool
 *     String : les modificatinos ont été appliquées
 *     false si aucun des critères n'est trouvé
**/
function lei_get_xml_criteres($item, $search) {
	$tt = ""; $ok = false;
	foreach ($search as $critere => $modas) {
		foreach ($modas as $moda => $texte) {
			if (false != $t = lei_get_xml_critere($item, $critere, $moda)) {
				$tt .= str_replace('@t@', $t, $texte);
				$ok = true;
			}
		}
	}
	if ($ok) {
		return $tt;
	}
	return false;
}


/**
 * Cherche parmis plusieurs tests de criteres de sélection
 *
 * @param string $item
 *     Arbre xml de l'élément...
 * @param array $search
 *     Critères que l'on souhaite sélectionner
 *     $search = array(
 *         $critere => array(
 *             $moda => "Texte à utiliser"))
 * 
 * @return string|bool
 *     Texte : un des critères a été trouvé
 *     false : aucun critère trouvé
**/
function lei_test_xml_criteres($item, $search) {
	$tt = ""; $ok = false;
	foreach ($search as $critere => $modas) {
		foreach ($modas as $moda => $texte) {
			if (lei_test_xml_critere($item, $critere, $moda)) {
				$tt .= $texte;
				$ok = true;
			}
		}
	}
	if ($ok) {
		return $tt;
	}
	return false;
}


?>
